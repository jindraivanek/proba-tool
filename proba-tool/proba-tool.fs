module ProbaTool

let tee f x = f x; x

module Option =
    let teeNone f o = match o with | Some _ -> o | None -> f(); o
    let attempt f x = try Some (f x) with _ -> None

open Argu
open FSharp.Data
open XPlot.Plotly
open MathNet.Numerics
open Thoth.Json.Net

type CreateArgs =
    | [<Mandatory>] CsvX of string * int
    | [<Mandatory>] CsvY of string * int
    | OutputCsv of string
    | Plot
    | FitBorders of float list
    | FitOrder of int
    | FitParamsFile of string
    | Cutoff_Z_Index of int
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | CsvX _ -> "input csv file and column number for X data."
            | CsvY _ -> "input csv file and column number for Y data."
            | OutputCsv _ -> "output csv file name for paired data."
            | Plot -> "show plots of paired data in default browser."
            | FitBorders _ -> "fit functions split points (on X axis)."
            | FitOrder _ -> "order of fitted polynoms."
            | FitParamsFile _ -> "file path where to save fit parameters"
            | Cutoff_Z_Index _ -> "z index for cutoff outliers (multiplier of standard deviation). Recommended value is 3."

type FitArgs =
    | [<Mandatory>] FitParamsFile of string
    | [<Mandatory>] FitInputCsv of string * int
    | FitOutputCsv of string
    | Precision of int
    | MinValue of float
    | MaxValue of float
    | Plot
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | FitInputCsv _ -> "input csv file and column number for data to be fitted."
            | FitOutputCsv _ -> "output csv file name for fitted data. If ommited, fitted data is written to stdout."
            | FitParamsFile _ -> "file path with fit parameters"
            | Precision _ -> "precision (decimal places) of fitted numbers"
            | MinValue _ -> "lower bound of fitted value (smaller values will be changed to this value)"
            | MaxValue _ -> "upper bound of fitted value (bigger values will be changed to this value)"
            | Plot -> "show plots of paired data in default browser."

type Args =
    | [<CliPrefix(CliPrefix.None)>] Create of ParseResults<CreateArgs>
    | [<CliPrefix(CliPrefix.None)>] Fit of ParseResults<FitArgs>
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Create _ -> "Create fitting parameters from set of real and expected dataset."
            | Fit _ -> "Apply fitting function on data."

let usageText = """
Typical usage:
1. Load data for X and Y axis and plot them to find functions border points:
    proba-tool.exe create --csvx proba_spacetime_coord_te-IRI.csv 6 --csvy proba_exp.csv 8 --outputcsv pairs.csv --plot
2. With functions border points, we can fit function by polynoms
    proba-tool.exe create --csvx proba_spacetime_coord_te-IRI.csv 6 --csvy proba_exp.csv 8 --fitborders 1781 3422 --fitorder 50 --fitparamsfile fitparams.json
3. And use fitted function to any data:
    proba-tool.exe fit --fitparamsfile fitparams.json --fitinputcsv fitinput.csv 1 --fitoutputcsv fit.csv
"""

let parseFloat (x: string) = 
    let (r,y) = System.Double.TryParse(x, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture) 
    if r then Some y else None

let getData (csvPath: string) (column: int) =
    use f = CsvFile.Load(uri=csvPath, separators=", ", hasHeaders=false, ignoreErrors=true)
    f.Rows |> Seq.choose (fun r -> 
        if r.Columns.Length < column then
            printfn "WARNING: row %A has only %i columns, %i expected" r r.Columns.Length column
            None
        else
        r.[column-1] |> parseFloat |> Option.teeNone (fun () -> printfn "WARNING: cannot parse input value %s on row %A" r.[column-1] r.Columns))
    |> Seq.toArray

let writeData (csvPath: string) pairs =
    pairs |> Seq.map (fun (x, y) -> x.ToString() + "," + y.ToString())
    |> fun rows -> System.IO.File.WriteAllLines(csvPath, rows)

let writeDataIntoCopy precision (csvPath: string) (column: int) (outputCsvPath: string) pairs =
    let d = pairs |> Map.ofSeq
    use f = CsvFile.Load(uri=csvPath, separators=", ", hasHeaders=false, ignoreErrors=true)
    use f2 = f.Map (fun r -> 
        r.Columns.[column - 1] |> parseFloat |> Option.bind (fun x -> d |> Map.tryFind x) |> Option.map (fun current ->
            let cols = Array.copy r.Columns
            let x = precision |> Option.map (fun (n: int) -> System.Math.Round((current: float), n)) |> Option.defaultValue current
            Array.set cols (column-1) (string x)
            CsvRow(f,cols))
        |> Option.defaultWith (fun () ->
            printfn "WARNING: missing fitted value or value parse error for value %s, row %A was not updated." r.Columns.[column - 1] r.Columns
            r))
    System.IO.File.Delete outputCsvPath
    f2.Save(outputCsvPath, separator=' ')


let getPairs xs ys =
    let xlen = Array.length xs 
    let ylen = Array.length ys 
    if xlen < ylen then
        xs |> Array.mapi (fun i x -> x, ys.[int <| (float i)*(float ylen)/(float xlen)])
    else
        ys |> Array.mapi (fun i y -> xs.[int <| (float i)*(float xlen)/(float ylen)], y)

let showPlot xLabel yLabel (pairs: array<float * float>) =
    let maxSize = 10000
    let factor = (pairs.Length / maxSize) + 1
    let data = pairs |> Seq.indexed |> Seq.filter (fun (i, _) -> i % factor = 0) |> Seq.map snd 
    writeData "plot.csv" data
    data
    |> Chart.Line
    |> Chart.WithTitle "X-Y pairs plot"
    |> Chart.WithXTitle xLabel
    |> Chart.WithYTitle yLabel
    |> Chart.Show

let showHistogram label (xs: array<float>) =
    let maxSize = 1000
    let min = Array.min xs
    let max = Array.max xs
    let range = max - min
    let f x = ((x - min) / range) * float maxSize |> int
    let revf x =  min + ((float x / float maxSize) * range)
    let counts = 
        xs |> Array.countBy f |> Array.sortBy fst
        |> Array.map (fun (x, y) -> x, revf x, log10 (float y))
    //counts |> Seq.iter (printfn "%A")
    counts
    |> Array.map (fun (a,b,c) -> b, c)
    |> Chart.Column
    |> Chart.WithTitle $"Histogram of {label}"
    |> Chart.WithXTitle "value"
    |> Chart.WithYTitle "log10(count)"
    |> Chart.Show


type FitParam = {
    UpperBound : float
    Polynoms : float []
}

type FitParams = FitParam []

module FitParam =
    let fitPoly' order ps =
        let ps = ps |> Seq.toArray
        let xs = ps |> Array.map fst |> Array.map float
        let ys = ps |> Array.map snd |> Array.map float
        let p = Fit.Polynomial(xs, ys, order)
        let f = Fit.PolynomialFunc(xs, ys, order) |> (fun f -> fun x -> f.Invoke(x))
        f, p

    let fitPolyParams n ps = fitPoly' n ps |> snd
    let fitPoly n ps = 
        let (f, p) = fitPoly' n ps
        fun x ->
            let y = Evaluate.Polynomial((x:float), p)
            assert (f x = y)
            f x

    let fitFuncs n parts = 
        parts |> Array.map (fun ps ->  
            fitPoly n ps)

    let fitParams n parts = 
        parts |> Array.map (fun ps ->  
            let p = fitPolyParams n ps
            p |> Seq.toArray)

    let getParts pairs limitPoints = 
        let pairsX = pairs |> Seq.map fst
        limitPoints |> Seq.pairwise |> Seq.map (fun (x1,x2) -> 
            let a = pairsX |> Seq.findIndex (fun x -> x >= x1)
            let b = pairsX |> Seq.findIndex (fun x -> x >= x2)
            printfn "%A" (a,b)
            pairs |> Seq.skip a |> Seq.take (b-a)) |> Seq.map Seq.toArray |> Seq.toArray

    let create n ps limitPoints =
        let parts = getParts ps limitPoints
        Seq.zip (Seq.skip 1 limitPoints) (fitParams n parts) |> Seq.map (fun (x,p) -> { UpperBound = x; Polynoms = p }) |> Seq.toArray

    let eval fitParams x =
        fitParams
        |> Seq.tryFind (fun p -> x <= p.UpperBound)
        |> Option.defaultValue (Seq.last fitParams)
        |> fun p -> Evaluate.Polynomial(x, p.Polynoms)

    let save fitParams path =
        fitParams |> Array.map (fun p -> 
            Encode.object [ "UpperBound", Encode.float p.UpperBound; "Polynoms", p.Polynoms |> Array.map Encode.float |> Encode.array ])
        |> Encode.array |> Encode.encode 1
        |> fun json -> System.IO.File.WriteAllText(path, json)

    let load path =
        let decoder = 
            Decode.decode
                (fun x y -> { UpperBound = x; Polynoms = y })
                |> Decode.required "UpperBound" Decode.float
                |> Decode.required "Polynoms" (Decode.array Decode.float)
            |> Decode.array
        match System.IO.File.ReadAllText path |> Decode.decodeString decoder with
        | Ok x -> x
        | Error e -> failwithf "Loading fit parameters failed with %s" e
        

[<EntryPoint>]
let main argv =
    let argsParser = Argu.ArgumentParser.Create<Args>(programName = "proba-tool.exe", helpTextMessage = usageText)
    try 
        let args = argsParser.Parse argv
        match args.GetSubCommand() with
        | Create args ->
            let (csvXPath, xcolumn) = args.GetResult <@ CsvX @>
            let (csvYPath, ycolumn) = args.GetResult <@ CsvY @>
            let xdata = getData csvXPath xcolumn |> Array.filter (fun x -> x >= 0.0) |> tee Array.sortInPlace
            printfn "X data loaded (%i rows)" xdata.Length
            let ydataPreFilter = getData csvYPath ycolumn |> Array.filter (fun x -> x >= 0.0)
            let ydata = 
                match args.TryGetResult <@ Cutoff_Z_Index @> with
                | Some z -> 
                    let stats = MathNet.Numerics.Statistics.DescriptiveStatistics(ydataPreFilter)
                    let cutOff = stats.Mean + (float z) * stats.StandardDeviation
                    printfn "Cutoff value is %f" cutOff
                    ydataPreFilter |> Array.filter (fun x -> x <= cutOff)
                | None -> ydataPreFilter
                |> tee Array.sortInPlace
            printfn "Y data loaded (%i rows)" ydata.Length
            let pairs = getPairs xdata ydata
            let pairsX = pairs |> Seq.map fst
            printfn "Data pairs created (%i rows)" pairs.Length
            args.TryGetResult <@ OutputCsv @> |> Option.iter (fun outputCsv ->
                writeData outputCsv pairs
                printfn "Data pairs written into %s" outputCsv)
            if args.Contains <@ CreateArgs.Plot @> then
                showPlot "X (original values)" "Y (expected values)" pairs
                showHistogram "X (original values) source" xdata
                showHistogram "Y (expected values) source" ydata
            if args.Contains <@ FitBorders @> then
                let limitPoints = [Seq.min pairsX] @ (args.GetResult <@ FitBorders @>) @ [Seq.max pairsX]
                let n = args.GetResult(<@ FitOrder @>, defaultValue = 50)
                let fitParams = 
                    printfn "Creating fit parameters"
                    FitParam.create n pairs limitPoints
                printfn "Fit polynoms coeficients:" 
                printfn "%A" fitParams

                args.TryGetResult <@ CreateArgs.FitParamsFile @> 
                |> Option.iter (tee (printfn "Saving fit parameters to %s.") >> FitParam.save fitParams)

        | Fit args ->
            let fitParamsPath = args.GetResult <@ FitArgs.FitParamsFile @>
            printfn "Loading fit parameters from %s." fitParamsPath
            let fitParams = FitParam.load fitParamsPath
            
            let applyBounds =
                let minValue = args.TryGetResult <@ MinValue @>
                let maxValue = args.TryGetResult <@ MaxValue @>
                let minF = minValue |> Option.map (fun m -> max m) |> Option.defaultValue id
                let maxF = maxValue |> Option.map (fun m -> min m) |> Option.defaultValue id
                minF >> maxF
            
            let fit x = 
                FitParam.eval fitParams x
                |> applyBounds
                
            let (fitInputCsv, col) = args.GetResult <@ FitInputCsv @>
            let fitInput = getData fitInputCsv col
            let fitOutput = fitInput |> Array.map (fun x -> 
                let y = fit x
                if y < 0.0000001 then
                    printfn "WARNING: fitted value %f from %f is too small, probably out of fit range" y x
                x, fit x)
            if args.Contains <@ Plot @> then
                showPlot "original values" "fitted values" (Array.sort fitOutput)
                showHistogram "X (original values) source" fitInput
                showHistogram "fitted values" (Array.map snd fitOutput)
            args.TryGetResult <@ FitOutputCsv @> |> Option.map (fun fitOutputCsv ->
                fun () ->
                    writeDataIntoCopy (args.TryGetResult <@ Precision @>) fitInputCsv col fitOutputCsv fitOutput
                    printfn "Fitted data written into %s" fitOutputCsv)
            |> Option.defaultValue (fun () -> fitOutput |> Seq.iter (printfn "%A"))
            |> fun f -> f()
    with
    | :? Argu.ArguParseException as e -> printfn "%s" e.Message
    | e -> printfn "%s: %s %s" (e.GetType().ToString()) e.Message e.StackTrace
        //argsParser.PrintUsage() |> printfn "%s"

    0 // return an integer exit code
